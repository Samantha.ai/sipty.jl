using SIPTY
using Test
using Distributed

@testset "Char/Spike conversion" begin
  str = "test 123"
  for format in (:dense, :sparse)
    for layout in (:bool, :bit)
      # Convert from string to spikes
      spikes_vec = []
      for char in str
        spikes = char_to_spikes(char; format=format, layout=layout)
        push!(spikes_vec, spikes)
      end

      # Convert from spikes to string
      _str = ""
      for spikes in spikes_vec
        char = spikes_to_char(spikes; format=format)
        _str *= char
      end
      @test str === _str
    end
  end
end
function roundtrip(cst::CharSpikeTracker)
  data = collect(cst)
  return CharSpikeString(data)
end
@testset "Char/Spike tracking" begin
  str1 = "hello\nworld!"
  cstr1 = CharSpikeString(str1)
  @test str1 == String(cstr1)
  # TODO: Test with all format and layout combos
  cst1 = CharSpikeTracker(cstr1)
  cstr1_out = roundtrip(cst1)
  @test all(cstr1 .== cstr1_out)
  @test_broken "test_push!"
end
@testset "PTY Handling" begin
  # Create PTY master
  ptym = PTYMaster()

  # Spawn echoing julia process as PTY slave
  wid = first(addprocs(1))
  remotecall(wid) do
    @eval using SIPTY
    ptys = PTYSlave(ptym.slave_name)
    str = readline(ptys)
    println(ptys, str)
  end

  # Send ping and wait for pong
  ping_str = "from master with love"
  println(ptym, ping_str)
  pong_str = readline(ptym)
  @test ping_str === pong_str

  # Finalize
  finalize(ptym.pty)
end
