module SIPTY

using Requires

include("pty.jl")
include("capture.jl")
include("char-spike.jl")

function __init__()
  include(joinpath(@__DIR__, "requires.jl"))
end

end # module
