export PTYMaster, PTYSlave

struct PTYMaster
  pty::IOStream
  slave_name::String
end
function PTYMaster(;fork_path=nothing, args=nothing)
  # FIXME: Use posix_openpt instead
  pty = open("/dev/ptmx", "r+")

  # Register finalizer for pty
  finalizer(pty) do pty
    close(pty)
  end

  # Allocate, grant, and unlock pty, and get slave name
  slave_name = try
    fdpty = fd(pty)
    res = ccall(:grantpt, Cint, (Cint,), fdpty)
    @assert res == 0 "grantpt failed"
    res = ccall(:unlockpt, Cint, (Cint,), fdpty)
    @assert res == 0 "unlockpt failed"
    unsafe_string(ccall(:ptsname, Cstring, (Int,), fdpty))
  catch err
    close(pty)
    rethrow(err)
  end
  pty_master = PTYMaster(pty, slave_name)

  if fork_path !== nothing
    # Fork and exec fork_path
    pty_fork(slave_name, fork_path, args...)
  end

  return pty_master
end
Base.print(ptym::PTYMaster, x...) =
  (print(ptym.pty, x...); flush(ptym.pty))
Base.println(ptym::PTYMaster, x...) =
  (println(ptym.pty, x...); flush(ptym.pty))
Base.readline(ptym::PTYMaster) = readline(ptym.pty)
Base.readavailable(ptym::PTYMaster) = readavailable(ptym.pty)
Base.flush(ptym::PTYMaster) = flush(ptym.pty)

struct PTYSlave
  pty::IOStream
  dev_path::String
end
PTYSlave(path::String) =
  PTYSlave(open(path, "r+"), path)
Base.println(ptys::PTYSlave, x...) =
  (println(ptys.pty, x...); flush(ptys.pty))
Base.readline(ptys::PTYSlave) = readline(ptys.pty)

function pty_fork(slave_name, fork_path, args...)
  argv = String[fork_path, args...]
  f = open(slave_name, "r+")
  pid = ccall(:fork, Cint, ())
  if pid < 0
    close(f)
    error("fork() failed")
  elseif pid == 0
    # Child, redirect IO and exec fork_path
    redirect_stdin(f)
    redirect_stdout(f)
    redirect_stderr(f)
    ccall(:execve, Cint, (Cstring, Ptr{Cstring}, Ptr{Cvoid}), fork_path, argv, Ref(C_NULL))
  end
end
