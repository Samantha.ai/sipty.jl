export capture_shell_session

"Sets up an interactive shell session and records all terminal IO"
function capture_shell_session()
  ptym = PTYMaster(;fork_path="/bin/bash", args=["-i"])
  println(ptym, "ls")
  run(`stty min 1`)
  in_task = @task begin
    while true
      #wait(stdin)
      bytes = read(stdin, 10)
      str = String(bytes)
      print("sent 1")
      # FIXME: Interpret/handle Ctrl-C
      # FIXME: Write to log
      print(ptym, str)
      flush(ptym)
    end
  end
  out_task = @task begin
    try
      while true
        #print("pass 1")
        #wait(ptym)
        eof(ptym.pty)
        #print("pass 2")
        bytes = readavailable(ptym)
        #print("pass 3")
        str = String(bytes)
        # FIXME: Write to log
        print("\e[2J\e[H")
        print(str)
      end
    catch err
      @error err
    end
  end
  schedule(in_task)
  schedule(out_task)
  wait(in_task)
  run(`tput reset`)
end
