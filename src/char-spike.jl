export CharSpikeData, CharSpikeString, CharSpikeTracker
export char_to_spikes, char_to_spikes!, spikes_to_char
export to_vec, from_vec, blank_vec
export test_spikes!

struct CharSpikeData{Arr<:AbstractArray}
  arr::Arr
  clip::Bool
  flush::Bool
  format::Symbol
  layout::Symbol
end
function CharSpikeData(csd=nothing; kwargs...)
  if csd !== nothing
    if haskey(kwargs, :arr)
      arr = kwargs[:arr]
    else
      arr = deepcopy(csd.arr)
    end
    clip = get(kwargs, :clip, csd.clip)
    flush = get(kwargs, :flush, csd.flush)
    format = get(kwargs, :format, csd.format)
    layout = get(kwargs, :layout, csd.layout)
  else
    arr = get(kwargs, :arr, nothing)
    clip = get(kwargs, :clip, false)
    flush = get(kwargs, :flush, false)
    format = get(kwargs, :format, :dense)
    layout = get(kwargs, :layout, :bool)
  end
  if arr === nothing
    if format == :dense
      width = 8
    elseif format == :sparse
      width = 128
    end
    if layout == :bool
      arr = zeros(Bool, width)
    elseif layout == :bit
      arr = BitArray(undef, width)
      fill!(arr, false)
    end
  end
  return CharSpikeData(arr, clip, flush, format, layout)
end
#CharSpikeData(csd::CharSpikeData{A} where A) =
#  CharSpikeData(arr=deepcopy(csd.arr), clip=csd.clip, flush=csd.flush, format=csd.format, layout=csd.layout)

"A flag indicating a flush of a character stream."
struct FlushFlag end

# A string possibly containing flush flags
const CharSpikeString = Vector{Union{Char,FlushFlag}}
function CharSpikeString(str::String; flushseq=:lf)
  cstr = CharSpikeString()
  flushstr = flushseq_to_str(flushseq)
  strs = split(str, flushstr)
  for (idx,_str) in enumerate(strs)
    for char in _str
      push!(cstr, char)
    end
    if idx < length(strs)
      push!(cstr, FlushFlag())
    end
  end
  return cstr
end
function CharSpikeString(seq::Vector{CharSpikeData{A}} where A; flushseq=:lf)
  cstr = CharSpikeString()
  flushstr = flushseq_to_str(flushseq)
  for data in seq
    if data.clip
      if !data.flush
        char = spikes_to_char(data)
        push!(cstr, char)
      else
        push!(cstr, FlushFlag())
      end
    end
  end
  return cstr
end
function String(cstr::CharSpikeString; flushseq=:lf)
  iob = IOBuffer()
  flushstr = flushseq_to_str(flushseq)
  for elem in cstr
    if elem isa Char
      print(iob, elem)
    elseif elem isa FlushFlag
      print(iob, flushstr)
    end
  end
  return rstrip(String(iob.data), '\0')
end
function flushseq_to_str(flushseq::Symbol)
  if flushseq == :lf
    return "\n"
  elseif flushseq == :cr
    return "\r"
  elseif flushseq == :crlf
    return "\r\n"
  else
    error("Invalid flushseq: $flushseq")
  end
end

function to_vec(csd::CharSpikeData)
  vcat(csd.arr, csd.clip, csd.flush)
end
# FIXME: Allow passing in csd OR flags
function from_vec(vec::AbstractVector{T}) where T
  CharSpikeData(copy(vec[1:256]), clip=vec[257], flush=vec[258])
end
function blank_vec(csd::CharSpikeData)
  if csd.layout === :bit
    arr = BitArray(length(csd.arr)+2)
    fill!(arr, false)
  else
    arr = zeros(Bool, length(csd.arr)+2)
  end
  return arr
end

"Tracks the generation of a string via spikes, and vice-versa."
mutable struct CharSpikeTracker{A}
  str::CharSpikeString
  pos::Int
  data::CharSpikeData{A}
end
CharSpikeTracker(cstr::CharSpikeString; kwargs...) =
  CharSpikeTracker(cstr, 1, CharSpikeData(;kwargs...))
"Convenience method for a non-flushed string."
CharSpikeTracker(str::String; kwargs...) =
  CharSpikeTracker(CharSpikeString(str); kwargs...)

# TODO: This is duplicated above
char_to_spikes(csd::CharSpikeData, c::Char) =
  CharSpikeData(csd, arr=char_to_spikes(c::Char;
    format=csd.format, layout=csd.layout
  ), clip=true, flush=false)
char_to_spikes(csd::CharSpikeData, f::FlushFlag) =
  CharSpikeData(csd, clip=true, flush=true)
function char_to_spikes(c; format=:dense, layout=:bool)
  if format == :dense
    width = 8
  elseif format == :sparse
    width = 128
  end
  if layout == :bool
    arr = zeros(Bool, width)
  elseif layout == :bit
    arr = BitArray(undef, width)
    fill!(arr, false)
  else
    error(layout)
  end
  char_to_spikes!(arr, c; format=format)
  return arr
end
char_to_spikes!(csd::CharSpikeData, c::Char) =
  char_to_spikes!(csd.arr, c::Char; format=csd.format)
function char_to_spikes!(arr, c::Char; format=:dense)
  if format == :dense
    # arr is 8 elements
    u64 = UInt64(c)
    for idx in 1:8
      arr[idx] = (u64 & 1<<(idx-1)) > 0
    end
  elseif format == :sparse
    # arr is 128 elements
    arr[Int8(c)] = true
  end
end

spikes_to_char(csd::CharSpikeData) =
  spikes_to_char(csd.arr; format=csd.format)
function spikes_to_char(arr; format=:dense)
  if format == :dense
    # arr is 8 elements
    u64 = UInt64(0)
    for idx in axes(arr, 1)
      u64 |= arr[idx] << (idx-1)
    end
    return Char(u64)
  elseif format == :sparse
    # arr is 128 elements
    idx = findfirst(arr)
    return Char(first(idx))
  end
end

function reset!(cst::CharSpikeTracker, flush=true)
  cst.pos = 1
  cst.data = CharSpikeData(cst.data, flush=flush)
end

function Base.iterate(cst::CharSpikeTracker, state=nothing)
  if cst.pos > length(cst.str)
    return nothing
  end
  if rand() < 0.5
    char = cst.str[cst.pos]
    cst.data = char_to_spikes(cst.data, char)
    cst.pos += 1
  else
    cst.data = CharSpikeData(cst.data; clip=false)
  end
  return (CharSpikeData(cst.data), state)
end
Base.IteratorSize(cst::CharSpikeTracker) =
  Base.SizeUnknown()
Base.eltype(::Type{CharSpikeTracker{A}}) where A =
  CharSpikeData{A}

function test_spikes!(cst::CharSpikeTracker{A}, vec::A) where A<:AbstractVector
  arr = vec[1:256]
  clip = vec[257]
  flush = vec[258]
  if clip === false
    # Not ready
    return nothing
  end
  # Flush and char are mutually exclusive, I guess
  if flush
    char = FlushFlag()
  else
    if cst.format === :sparse
      len = length(findall(arr))
      if len !== 1
        # Bad response
        return false
      end
      # Valid char
      char = spikes_to_char(arr; format=:sparse)
    elseif cst.format === :dense
      # Always a valid char
      char = spikes_to_char(arr; format=:sparse)
    end
  end
  char_target = cst.str[cst.pos]
  if char === char_target
    # Advance
    cst.pos += 1
    if cst.pos > length(cst.data.arr)
      # Reset pos on wraparound
      cst.pos = 1
    end
    return true
  end
  return false
end
